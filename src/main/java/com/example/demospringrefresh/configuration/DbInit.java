package com.example.demospringrefresh.configuration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.example.demospringrefresh.domain.CreditAccount;
import com.example.demospringrefresh.domain.DebitAccount;
import com.example.demospringrefresh.domain.User;
import com.example.demospringrefresh.respositry.CreditAccountRepository;
import com.example.demospringrefresh.respositry.DebitAccountRepository;
import com.example.demospringrefresh.servcie.UserRepository;

@Component
public class DbInit {

    @Autowired
    CreditAccountRepository creditAccountRepository;

    @Autowired
    DebitAccountRepository debitAccountRepository;

    @Autowired
    UserRepository userRepository;

    @PostConstruct
    @Transactional
    public void initDb() {
        User user = new User();
        user.setPersonalId("123456");
        user.setName("John");
        user.setSurname("Bush");

        CreditAccount creditAccount = new CreditAccount();
        creditAccount.setDetails("First credit account details");
        creditAccount.setBalance(new BigDecimal("1000.00"));
        creditAccount.setAccountNumber("UNLA12345");
        creditAccount.setUser(user);

        CreditAccount creditAccount2 = new CreditAccount();
        creditAccount2.setDetails("Second credit account details");
        creditAccount2.setBalance(new BigDecimal("8000.00"));
        creditAccount2.setAccountNumber("UNLA67890");
        creditAccount2.setUser(user);

        List<CreditAccount> creditAccounts = new ArrayList<>();
        creditAccounts.add(creditAccount);
        creditAccounts.add(creditAccount2);
        creditAccountRepository.saveAll(creditAccounts);

        DebitAccount debitAccount = new DebitAccount();
        debitAccount.setDetails("First debit account details");
        debitAccount.setBalance(new BigDecimal("500.00"));
        debitAccount.setAccountNumber("SWX12345");
        debitAccountRepository.save(debitAccount);

        debitAccount.setUser(user);
        debitAccountRepository.save(debitAccount);

        User user2 = new User();
        user2.setPersonalId("67890");
        user2.setName("Mark");
        user2.setSurname("Wick");
        userRepository.save(user2);

        DebitAccount debitAccount2 = new DebitAccount();
        debitAccount2.setDetails("Second debit acount details");
        debitAccount2.setBalance(new BigDecimal("700.00"));
        debitAccount2.setAccountNumber("SWX67890");
        debitAccountRepository.save(debitAccount2);
        debitAccount2.setUser(user2);
        debitAccountRepository.save(debitAccount2);
    }
}
