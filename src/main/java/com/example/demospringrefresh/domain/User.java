package com.example.demospringrefresh.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String surname;

    @Column(unique=true)
    private String personalId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<CreditAccount> creditAccounts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<DebitAccount> debitAccounts;


    public List<CreditAccount> getCreditAccounts() {
        return creditAccounts;
    }

    public void setCreditAccounts(List<CreditAccount> creditAccounts) {
        this.creditAccounts = creditAccounts;
    }

    public List<DebitAccount> getDebitAccounts() {
        return debitAccounts;
    }

    public void setDebitAccounts(List<DebitAccount> debitAccounts) {
        this.debitAccounts = debitAccounts;
    }







    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }
}
