package com.example.demospringrefresh.respositry;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demospringrefresh.domain.CreditAccount;

public interface CreditAccountRepository extends CrudRepository<CreditAccount, Long> {

    List<CreditAccount>  findAll();
    List<CreditAccount>  findAllByUserPersonalId(String personalId);

    CreditAccount findByAccountNumber(String accountNumber);

    @Query("select c from CreditAccount c where c.accountNumber = ?1")
    CreditAccount myNativeSearchByJPQL(String accountNumber);

    @Query(value= "select * from credit_account c where c.account_number = ?1", nativeQuery = true)
    CreditAccount myNativeSearchByNativeSql(String accountNumber);


}
