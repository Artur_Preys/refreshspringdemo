package com.example.demospringrefresh.respositry;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demospringrefresh.domain.CreditAccount;
import com.example.demospringrefresh.domain.DebitAccount;

public interface DebitAccountRepository extends CrudRepository<DebitAccount, Long> {

    List<DebitAccount> findAll();

    List<DebitAccount>  findAllByUserPersonalId(String personalId);

    DebitAccount findByAccountNumber(String accountNumber);

    @Query("select c from DebitAccount c where c.accountNumber = ?1")
    DebitAccount myNativeSearchByJPQL(String accountNumber);

    @Query(value= "select * from debit_account c where c.account_number = ?1", nativeQuery = true)
    DebitAccount myNativeSearchByNativeSql(String accountNumber);


}
