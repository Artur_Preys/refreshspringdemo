package com.example.demospringrefresh.servcie;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demospringrefresh.domain.CreditAccount;
import com.example.demospringrefresh.domain.DebitAccount;
import com.example.demospringrefresh.respositry.CreditAccountRepository;
import com.example.demospringrefresh.respositry.DebitAccountRepository;

@Component
public class AccountService {

    private CreditAccountRepository creditAccountRepository;
    private DebitAccountRepository debitAccountRepository;

    @Autowired
    public AccountService(CreditAccountRepository creditAccountRepository, DebitAccountRepository debitAccountRepository) {
        this.creditAccountRepository = creditAccountRepository;
        this.debitAccountRepository = debitAccountRepository;
    }

    public void addToCredit(String accountNumber, BigDecimal amount) {
        CreditAccount creditAccount = creditAccountRepository.myNativeSearchByJPQL(accountNumber);
        creditAccount.setBalance(creditAccount.getBalance().add(amount));
        creditAccountRepository.save(creditAccount);
    }

    public void minusFromCredit(String accountNumber, BigDecimal amount) {
        CreditAccount creditAccount = creditAccountRepository.myNativeSearchByJPQL(accountNumber);
        creditAccount.setBalance(creditAccount.getBalance().subtract(amount));
        creditAccountRepository.save(creditAccount);

    }
    public void addToDebit(String accountNumber, BigDecimal amount) {
        DebitAccount debitAccount = debitAccountRepository.findByAccountNumber(accountNumber);
        debitAccount.setBalance(debitAccount.getBalance().add(amount));
        debitAccountRepository.save(debitAccount);
    }
    public void minusFromDebit(String accountNumber,BigDecimal amount) {
        DebitAccount debitAccount = debitAccountRepository.findByAccountNumber(accountNumber);
        debitAccount.setBalance(debitAccount.getBalance().subtract(amount));
        debitAccountRepository.save(debitAccount);

    }
    public BigDecimal getCreditAccountBalance(String accountNumber) {
        CreditAccount account= creditAccountRepository.myNativeSearchByNativeSql(accountNumber);
       return account.getBalance();
    }

    public BigDecimal getDebitAccountBalance(String accountNumber) {
       DebitAccount debitAccount = debitAccountRepository.findByAccountNumber(accountNumber);
       return debitAccount.getBalance();
    }



}
