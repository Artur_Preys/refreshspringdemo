package com.example.demospringrefresh.servcie;

import org.springframework.data.repository.CrudRepository;

import com.example.demospringrefresh.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByPersonalId(String personalId);
}
