package com.example.demospringrefresh.servcie;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demospringrefresh.respositry.CreditAccountRepository;
import com.example.demospringrefresh.respositry.DebitAccountRepository;

@Component
public class ReportService {

    @Autowired
    CreditAccountRepository creditAccountRepository;
    @Autowired
    DebitAccountRepository debitAccountRepository;

    public BigDecimal getCreditAccountsBalance(){
        return new BigDecimal(creditAccountRepository.findAll()
            .stream()
            .map(it-> it.getBalance())
            .mapToDouble(BigDecimal::doubleValue).sum());

    }
    public BigDecimal getDebitAccountsBalance(){
        return new BigDecimal(debitAccountRepository.findAll()
            .stream()
            .map(it-> it.getBalance())
            .mapToDouble(BigDecimal::doubleValue)
            .sum());
    }
    public void printUserBalance(String personalId){
        BigDecimal result =  new BigDecimal( creditAccountRepository.findAllByUserPersonalId(personalId).stream()
            .map(it-> it.getBalance())
            .mapToDouble(BigDecimal::doubleValue).sum());

        BigDecimal debitResult =  new BigDecimal( debitAccountRepository.findAllByUserPersonalId(personalId).stream()
            .map(it-> it.getBalance())
            .mapToDouble(BigDecimal::doubleValue).sum());
        System.out.println("User credit amount is" + result);
        System.out.println("User debit amount is" + debitResult);
    }

}
