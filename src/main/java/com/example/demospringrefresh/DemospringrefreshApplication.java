package com.example.demospringrefresh;

import java.math.BigDecimal;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.example.demospringrefresh.domain.CreditAccount;
import com.example.demospringrefresh.domain.DebitAccount;
import com.example.demospringrefresh.domain.User;
import com.example.demospringrefresh.respositry.CreditAccountRepository;
import com.example.demospringrefresh.respositry.DebitAccountRepository;
import com.example.demospringrefresh.servcie.AccountService;
import com.example.demospringrefresh.servcie.ReportService;
import com.example.demospringrefresh.servcie.TestService;
import com.example.demospringrefresh.servcie.UserRepository;

import sun.nio.cs.US_ASCII;

@SpringBootApplication
public class DemospringrefreshApplication implements CommandLineRunner {

    @Autowired
    CreditAccountRepository creditAccountRepository;

    @Autowired
    DebitAccountRepository debitAccountRepository;

    @Autowired
    AccountService accountService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ReportService reportService;

    public static void main(String[] args) {
        SpringApplication.run(DemospringrefreshApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {


     /*   CreditAccount creditAccount = new CreditAccount();
        creditAccount.setBalance(new BigDecimal("123"));
        User user = new User();
        user.setPersonalId("1231");
        user.setCreditAccounts(Arrays.asList(creditAccount));

        creditAccount.setUser(user);

        userRepository.save(user);
        creditAccountRepository.save(creditAccount);*/

        User user = userRepository.findByPersonalId("123456");
        user.getCreditAccounts().forEach(it-> System.out.println(it.getBalance()));
        user.getDebitAccounts().forEach(it-> System.out.println(it.getBalance()));

    //    User userFromDb = userRepository.findByPersonalId("123456");
      //  userFromDb.getCreditAccounts().forEach(it-> System.out.println( it.getDetails()));
       // userFromDb.getDebitAccounts().forEach(it-> System.out.println( it.getDetails()));


      /*  accountService.addToCredit("UNLA12345", new BigDecimal("100"));
        accountService.minusFromCredit("UNLA12345", new BigDecimal("200"));
        accountService.addToDebit("SWX12345", new BigDecimal("200"));
        accountService.minusFromDebit("SWX12345", new BigDecimal("400"));
*/
       // System.out.println("Credit balance = " + accountService.getCreditAccountBalance("UNLA12345"));
       // System.out.println("Debit balance = " + accountService.getDebitAccountBalance("SWX12345"));

        System.out.println(reportService.getCreditAccountsBalance());

        reportService.printUserBalance("123456");

    }
}
